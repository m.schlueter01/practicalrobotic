# Vorschlag zum Algorithmus

## Umschreibung

1. Folge SP
2. Wenn du auf einen Block triffst:
  - Block blau:
    - Vergiss alle ausgehenden / blockierten Verbindungen von diesem Knoten und berechne SP neu
    - Gehe zu 1.
  - Block rot:
    - Finde alle angrenzenden Knoten als Kandidaten zum Schieben.
    - Von diesen <= 4 Knoten, von welchen aus kann ich tatsaechlich schieben?
    - Wie lang ist jeweils der SP=:SP1 dahin ohne den Block zu beruehren?
    - Angenommen, ich habe den Block von dort geschoben, wie lang ist dann der neue SP=:SP2 von dort zum Ziel G?
    - Von allen SP1+SP2, welches ist der kuerzeste Weg?
    - Nimm diesen
    - Gehe zu 1.

## Detailed

1. Setze `pos_curr` <-- `S`
2. Calc. `path` <-- `SP(graph, curr_pos, G)`
3. Setze `pos_next` <-- `path[1]`
4. Follow path:
   - 4.1 Turn in direction of edge
   - 3.2 Follow edge
   - 3.3 identify node
   - 3.4 stop at node
   - 3.5 `curr_pos` <-- pos
   - 3.6
      - 3.6.1 If no block:
        - Go to 2.
      - 3.6.2 else:
        - 3.6.2.1 if block is blue (not touchable):
          - 3.6.2.1.1 Delete all adjacent edges of `curr_pos` but the traveled one (<= 3) from `graph`
          - 3.6.2.1.2 Go to 2.
        - 3.6.2.2 else (block is red, movable):
          - Find all possibilities to move the block (<= 4):
            - Take all adj. nodes `tmp_list`=[$`pos_1, \ldots, pos_N`$] of `curr_pos` ($`(2 \le N \le 4)`$, otherwise it wouldn't have been SP)
            - `possibilities` = []
            - Take $`pos_1`$ and test whether it lies on a straight line with one of $`pos_2, \ldots, pos_N`$
              - If not:
                - If $`N=2`$: (Block is not movable)
                  - Go to 3.6.2.1.1
                - If $`N=3`$:
                  - $`pos_2`$ and $`pos_3`$ lie on a straight line.
                  - add tupel ($`pos_2`$, $`pos_3`$) to list `possibilities`
              - else ($`pos_1`$ and $`pos_i`$ lies on a straight line):
                - If $`N=4`$: Add The remaining two nodes ($`pos_j`$, $`pos_k`$) to list `possibilities` (lie on a straight line as well)
                - If $`N=3`$: Nothing to do (remaining node not pushable from)
                - If $`N=2`$: Nothing to do
              - |`possibilities`| $`\in \{0,1,2\}`$ (Tuples of Nodes the block can be moved from)
          - If |`possibilities`| == 0: Go to 3.6.2.1.1
          - `graph_copy` = `graph`
          - Delete all adjacent edges of `curr_pos` but the traveled one (<= 3) from `graph_copy`
          - `total_steps` = []
          - for ($`pos_j`$, $`pos_k`$) $`\in`$ `possibilities`:
            - Calculate total steps of new path for pushing block to $`pos_k`$ coming from $`pos_j`$:
              - Calc. $`N_j = |SP(graph\_copy, curr\_pos, pos_j)|`$ (path lengths to positions / directions where the block can be pushed from)
              - `graph_copy2` = `graph`
              - Delete all adjacent edges of $`pos_k`$ but the one from `curr\_pos` (<= 3) from `graph_copy2` (case: block had been pushed from `curr_pos` to $`pos_k`$)
              - Calc. $`M_j = |SP(graph\_copy, curr\_pos, G)| + 3`$ (new SP to G; +3: One for the move from $`pos_j`$ to `curr_pos`, two for pushing from `curr_pos` to $`pos_k`$ and going back)
              - `total_steps` += ($`N_j + M_j`$, $`pos_j`$)
            - Calculate total steps of new path for pushing block to $`pos_j`$ coming from $`pos_k`$:
              - Calc. $`N_k = |SP(graph\_copy, curr\_pos, pos_k)|`$ (path lengths to positions / directions where the block can be pushed from)
              - `graph_copy2` = `graph`
              - Delete all adjacent edges of $`pos_j`$ but the one from `curr\_pos` (<= 3) from `graph_copy2` (case: block had been pushed from `curr_pos` to $`pos_j`$)
              - Calc. $`M_k = |SP(graph\_copy, curr\_pos, G)| + 3`$ (new SP to G; +3: One for the move from $`pos_k`$ to `curr_pos`, two for pushing from `curr_pos` to $`pos_j`$ and going back)
              - `total_steps` += ($`N_k + M_k`$, $`pos_k`$, `graph_copy2`)
          - `tmp_node` = node with smallest length of `total_steps`
          - `path` = `SP(graph_copy, curr_pos, tmp_node)` + `(tmp_node, curr_pos)` + ein weiter (schieben) + ein zurueck zu (`curr_pos`) + `SP(graph_copy2, curr\_pos, G)`
          - Go to 3.
