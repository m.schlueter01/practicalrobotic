from . import maze_library as ml
import networkx as nx
import copy
class path_algo:
	def __init__(self):
		self.maze, self.graph, self.P = ml.read_maze('maze_test.ascii', test=True)

	def getPath(self,pos_curr):
		return ml.shortest_path(self.graph, pos_curr, self.P['G'])

	def findBlock(self,pos_last,pos_curr,block_status):
		if block_status is None:
			pass

		elif block_status == 'B':
			graph, maze = ml.delete_adj_edges(self.graph, pos_last, pos_curr, self.maze, verbose=True)
			path = ml.shortest_path(graph, pos_curr, self.P['G'], verbose=True)

		elif block_status == 'R':
			neighbours = ml.find_neighbours(self.graph, pos_curr, verbose=True)
			candidates = []
					
			# Find opposite nodes
			if len(neighbours) == 2:
				
				if ml.points_on_straight_line( *neighbours ):
					candidates = [ tuple(neighbours) ]
				# Block is not movable
				else:
					graph, maze = ml.delete_adj_edges(self.graph, pos_last, pos_curr, self.maze, verbose=True)
					path = ml.shortest_path(graph, pos_curr, self.P['G'], verbose=True)
					
			if len(neighbours) == 3:
			
				for p in neighbours[1:]:
					if self.points_on_straight_line(neighbours[0], p):
						candidates = [ (neighbours[0], p) ]
						
				if candidates == []:
					candidates = [ tuple(*neighbours[1:]) ]
					
			if len(neighbours) == 4:
				
				for ppp, p in enumerate(neighbours[1:]):
					
					if self.points_on_straight_line(neighbours[0], p):
						candidates.append( (neighbours[0], p) )
						candidates.append( tuple([ n for nnn, n in enumerate(neighbours[1:]) if nnn != ppp ]) )
						print('Found opposite node pair:', candidates[-2], candidates[-1])
						break
						
			distances = []
			# E.g: c = (neighbours[0], p) 
			for c in candidates:
				
				# Delete blocked edges temporary
				graph_tmp, _ = self.delete_adj_edges(copy.deepcopy(graph), pos_last, pos_curr, verbose=True)
				
				for iii in range(2):

					# From pos_curr to push position
					try:
						print('\nPushing from {} to {}'.format(*c))
						# Find path to start pushing
						# .. skipt if start pushing position is pos_last
						if pos_last == c[iii]:
							path = []
						else:
							path = ml.shortest_path(graph_tmp, pos_curr, c[iii], verbose=True)
						# Push and go back
						path += [ pos_curr, c[(iii+1)%2], pos_curr ]
						# Walk the rest
						path += ml.shortest_path(graph, pos_curr, self.P['G'], verbose=True)
						print(path)
						
						distances.append( [(c[iii], c[(iii+1)%2]), len(path)-1, path, pos_last] )
						
					# Disqualify path if no connection exists
					except nx.NetworkXNoPath:
						pass
					
				# No pushing at all
				try:
					path = ml.shortest_path(graph_tmp, pos_curr, self.P['G'], verbose=True)
					# None is placeholder for not pushing at all / it's best not to push
					distances.append( [None, len(path)-1, path, pos_last] )
				except nx.NetworkXNoPath:
						pass
			
			# Sort for distances / find shortest path
			distances = sorted(distances, key=lambda x: x[1])
			
			# Verbose debugging output
			# TODO: Remove after debugging
			for d in distances:
				if d[0] is not None:
					print('\n {} steps for push from {} to {},\npath: {}'.format(d[1], *d[0], d[2]))
				else:
					print('\n {} steps for not pushing,\npath: {}'.format(d[1], d[2]))
			
			# Take shortest path / best solution
			tmp_pos_pair, tmp_N, path, pos_last = distances[0]
			
			# If pushing ...
			if tmp_pos_pair is not None:
				# Update position R
				self.P['R']  = [ tmp_pos_pair[1] if x == pos_curr else x for x in self.P['R'] ]
				print('Updated P[R]:', self.P['R'])
				print('\nDecided to push the red block from {} to {}.'.format(*tmp_pos_pair))
			else:
				print('\nDecided not to push')
			print('Total steps to path: {}', tmp_N)
			print('Decided path to go:', path)

			return path