from matplotlib import pyplot as plt
import maze_library as ml
import matplotlib
import numpy as np
#matplotlib.use('TkAgg')

# %%
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

# %%
maze_orig, graph, P_orig = ml.read_maze('../maze_test.ascii', test=True)
P = P_orig
P['B'] = [ml.position((6,5),P['S'].shape)]
path_taken = [(6,0),(6,1),(6,2),(6,3),(6,4),(6,5),(6,4),(6,3),(6,2),(6,1),(6,0),(5,0),(4,0),(4,1),(4,2),(4,3),(4,4),(4,5),(4,6),(5,6),(6,6)]

# %%

fig, ax = plt.subplots(figsize=(3, 3.5))


def transform(x, y):
    return y, -x

# Plot maze
ax.scatter(*transform(*np.where(maze_orig == 1)), color='black')
ax.set_xticks([]), ax.set_yticks([])
ax.axis('equal')


def plot_block(position, label, color, ax=ax, alpha=1, marker='o'):
    ax.scatter(*transform(*position.unravel), linewidths=10, color=color, alpha=alpha, marker=marker)
    ax.text(*transform(*position.unravel), label, color='white', ha='center', va='center')


# # .. path
x, y = transform(*np.array(list(zip(*path_taken))))
ax.quiver(x[:-1], y[:-1], x[1:] - x[:-1], y[1:] - y[:-1], scale_units='xy', angles='xy', scale=1.1, width=.005,
          alpha=.5)

# # .. Start and Goal
plot_block(P['S'], 'S', 'green')

plot_block(P['G'], 'G', 'orange')

# .. original red block positions
for p in P_orig['R']:
    plot_block(p, '', 'red', alpha=.3, marker='s')

# .. current red block positions
for p in P['R']:
    plot_block(p, '', 'red', marker='s')

# .. current blue block positions
for p in P['B']:
    plot_block(p, '', 'blue', marker='s')

#plt.show()
fig.savefig('algorithm_deadend.pgf')
