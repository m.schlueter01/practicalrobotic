import numpy as np
from pathlib import Path
import networkx as nx
from copy import deepcopy

class maze_library:
	def __init__(self):
		return


# %%

class position:
    """
    A class to store raveled and unraveled representations of a position

    INIT:
        pos <int / iterable>: Position raveled or unraveled (maze coordinates)
        shape <np.array / tuple>:      Reverence shape for un-/raveling

    VALUES:
        self.ravel <int>           graph "coordinates" / node number
        self.unravel <(int, int)>  Maze coordinates
        self.shape <(int, int)>:   Dimensions of maze
    """

    def __init__(self, pos, shape):

        if isinstance(shape, tuple):
            self.shape = shape
        else:
            self.shape = tuple(shape.shape)

        if isinstance(pos, (int, np.integer)):
            self.ravel = int(pos)
            self.unravel = tuple(np.unravel_index(pos, self.shape))

        elif isinstance(pos, tuple):
            self.unravel = pos
            self.ravel = int(self.__ravel(pos))

        else:
            raise ValueError("pos need to be int or tuple")

    def __ravel(self, x):
        return x[0] * self.shape[1] + x[1]

    def __repr__(self):
        return 'P{{{:>5}, {:>3}}}'.format('({},{})'.format(*self.unravel), self.ravel)

    def __eq__(self, other):
        return True if other.ravel == self.ravel else False


# %%

def read_maze(filename='maze_orig.ascii', test=True):
    """
    Reads maze from ascii file
    Returns maze as binary matrix, adjacency matrix as well as ..
    .. start and stop positions.

    INPUT:

        filename <str>: Text file consisting ' ', '*' and '\n'
        test <bool>:    If true, "filename" might also contain Rs and Bs that will be ..
                        .. replaced by 1s and added to "P".

    OUTPUT:

        mase [np.array]: Array consisting of ints 0 and 1
        A <np.array>:    Adjacency matrix of maze
        P <{str: position / [position, ...]}>: Positions of "S"tart / "G"oal <int> as counted from top left ..
                                .. in test mode also positions of "R"ed and "B"lue blocks <list>
    """
    import numpy as np
    from pathlib import Path
    get_coordinates = lambda maze, letter: list(zip(*np.where(maze == letter)))

    maze = Path(filename).read_text()
    maze = maze.replace(' ', "0").replace('*', "1").strip().split('\n')

    # Fill rows with trailing spaces that had been cut off
    # .. (if line did non end with *)
    maze_width = max([len(row) for row in maze])
    for ii, row in enumerate(maze):
        if len(row) != maze_width:
            maze[ii] += (maze_width - len(row)) * '0'

    # To numpy array
    maze = np.array([[char for char in row] for row in maze])

    # Replace Start and Goal symbols with path elements
    S = get_coordinates(maze, 'S')[0]
    G = get_coordinates(maze, 'G')[0]
    maze[S] = 1
    maze[G] = 1

    # For dry testing of the algorithm, there might also be R and B
    if test is True:
        R = get_coordinates(maze, 'R')
        B = get_coordinates(maze, 'B')
        for (i, j) in R:
            maze[i, j] = 1
        for (i, j) in B:
            maze[i, j] = 1

    maze = maze.astype(int)

    # Adjacency matrix needed to create networkx graph
    A = np.zeros([np.prod(maze.shape), np.prod(maze.shape)])
    for row in range(h := maze.shape[0]):
        for col in range(w := maze.shape[1]):
            if maze[row, col] == 1:
                # Right neighbor
                if col + 1 < w and maze[row, col + 1] == 1:
                    A[row * w + col, row * w + col + 1] = 1
                    A[row * w + col + 1, row * w + col] = 1
                # North neighbor
                if row > 0 and maze[row - 1, col] == 1:
                    A[row * w + col, (row - 1) * w + col] = 1
                    A[(row - 1) * w + col, row * w + col] = 1

    P = {'S': position(S, maze), 'G': position(G, maze)}

    if test is True:
        P['B'] = [position(b, maze) for b in B]
        P['R'] = [position(r, maze) for r in R]

    graph = nx.convert_matrix.from_numpy_matrix(A)

    # A can from now on be retrieved using nx.adjacency_matrix(graph)
    return maze, graph, P


# %%

def shortest_path(graph, S, G, verbose=False):
    """
    Calculates shortest path from point S to G on basis of adjacency matrix A

    INPUT:

        graph <nx.graph>: Corresponding graph to maze
        S, G <position>:  Position of Start / Goal as counted from top left

    OUTPUT:

        positions <[position, ...]>: List of positions to go
    """
    import numpy as np

    path = nx.shortest_path(graph, S.ravel, G.ravel)

    # First one is S
    path = [position(n, S.shape) for n in path][1:]

    if verbose is True:
        print('Found SP (from {} -> {}):'.format(S, G), path)
    return path


# %%

def calc_edge(pos_curr, pos_next):
    """
    Calculate edge in relative maze-indices

    INPUT:
        pos_curr <(int, int)>: "maze" coordinates
        pos_next <(int, int)>: "maze" coordinates

    OUTPUT:
        edge <[np.ndarray, np.ndarray]>: Edge [pos_curr, pos_next]
    """

    return [pos_curr, pos_next]


# %%

def robot_align_edge(pos_last, pos_curr, pos_next):  # pos_curr, pos_next):
    """
    Align robot to edge

    INPUT:
        pos_curr <(int, int)>: "maze" coordinates
        pos_next <(int, int)>: "maze" coordinates

    OUTPUT:
        Nothing
    """


#     import numpy as np

#     try:
#         _ = iter(pos_curr)
#     except:
#         raise ValueError(f"{pos_curr} not an iterable")
#     try:
#         _ = iter(pos_next)
#     except:
#         raise ValueError(f"{pos_next} not an iterable")
#     pos_curr = np.array(pos_curr)
#     pos_next = np.array(pos_next)
# Turn robot

# %%

def robot_follow_edge(pos_last, pos_curr, pos_next):
    """
    Align robot to edge

    INPUT:
        edge <np.array([change_y, change_x])>
    OUTPUT:
        Nothing
    """

    # Move robot
    def identify_node():
        pass


# %%

def robot_identify_block(pos_curr, P, verbose=False):
    """
    Checks presence and if, color of block

    TEST-INPUT:
        pos_curr, P, debug

    INPUT:
        Nothing

    OUTPUT:
        status <None / str>: None / 'B' / 'R'
    """
    if pos_curr in P['B']:
        if verbose is True:
            print('Found B at', pos_curr)
        return 'B'
    elif pos_curr in P['R']:
        if verbose is True:
            print('Found R at', pos_curr)
        return 'R'
    else:
        if verbose is True:
            print('No block found at', pos_curr)
        return None


# %%

def delete_adj_edges(graph, pos_last, pos_curr, maze=None, verbose=False):
    """
    Delete all adjacent edges from pos_curr but the one to pos_last ...
    ... from graph, A and maze.

    INPUT:
        graph <nx.graph>: Graph
        maze <np.array>:  Array consisting of ints 0 and 1
        pos_last <position>: "maze" coordinates
        pos_curr <position>: "maze" coordinates

    OUTPUT:
        graph <nx.graph>: Graph
        maze <np.array>:  Array consisting of ints 0 and 1
    """
    # Update maze
    # None == out of shape
    if verbose is True:
        print("Remove from graph all adj. nodes of {} but {}".format(pos_curr, pos_last))

    if maze is not None:
        for p in [position((pos_curr.unravel[0] - 1, pos_curr.unravel[1]), maze) if pos_curr.unravel[0] >= 1 else None,
                  position((pos_curr.unravel[0] + 1, pos_curr.unravel[1]), maze) if pos_curr.unravel[0] < maze.shape[
                      0] - 2 else None,
                  position((pos_curr.unravel[0], pos_curr.unravel[1] - 1), maze) if pos_curr.unravel[1] >= 1 else None,
                  position((pos_curr.unravel[0], pos_curr.unravel[1] + 1), maze) if pos_curr.unravel[1] < maze.shape[
                      1] - 2 else None]:
            if p is not None and p.ravel != pos_last.ravel and maze[p.unravel] == 1:
                maze[p.unravel] = 0
                if verbose is True:
                    print("Removing from maze: {}".format(p))

    # Update graph
    for (i, j) in deepcopy(graph).edges(pos_curr.ravel):
        if pos_last.ravel not in (i, j):
            graph.remove_edge(i, j)
            if verbose is True:
                print("Removing from graph edge: ({},{})".format(i, j))
            # A[i,j] = 0

    return graph, maze


# %%

def find_neighbours(graph, pos, verbose=False):
    """
    INPUT:
        graph <nx.graph>: Graph
        pos <position>:   Position

    OUTPUT:
        <[ (int, int), ... ]>: Neighbours of position in "maze" coordinates


    """
    neighbours = [position(n, pos.shape) for n in graph[pos.ravel]]
    if verbose is True:
        print('Found neighbours:', neighbours)

    return neighbours


# %%

def points_on_straight_line(pos1, pos2):
    pos1 = np.array(pos1.unravel)
    pos2 = np.array(pos2.unravel)
    return True if len(np.nonzero(pos1 - pos2)[0]) == 1 else False


# %%

def follow_edge(pos_last, pos_curr, pos_next, verbose=False):
    # 4.1
    # robot_align_edge(pos_last, pos_curr, pos_next)

    # 4.2-4
    # robot_follow_edge(pos_curr, pos_next)
    if verbose:
        print('===> Going from {} to {}.'.format(pos_curr, pos_next))
    pass


# %%

if __name__ == "__main__":
    maze, A, P = read_maze('maze_test.ascii')
    S, G = P['S'], P['G']
#     path = shortest_path(graph, S, G)