"""
handling of all sensor related operations
"""

import time

WA_THRESHOLD   = 50
FEAT_THRESHOLD = 60

class SensorController:
    """
    controlls all sensor related aspects
    """
    def __init__(self, array, color_sens, ultra_sens, imu, debug):
        self.array = array
        self.color_sens = color_sens
        self.ultra_sens = ultra_sens
        self.imu = imu
        self.debug = debug

        array.mode = 'CAL'
        color_sens.mode = 'RGB-RAW'
        ultra_sens.mode = 'US-DIST-CM'

    def get_weighted_avg(self):
        """
        return weighted average of led array values to determine where line is
        """
        array_values = self.get_array_values()
        threshold_adjusted_value = [x - WA_THRESHOLD for x in array_values]
        bitmask = [1 if x <= 0 else 0 for x in threshold_adjusted_value]

        weighted_avg = 0
        detecting_sensors = 0
        for i in range(len(bitmask)):
            if bitmask[i] == 1:
                detecting_sensors += 1
            weighted_avg += bitmask[i] * (10*(i+1))

        if detecting_sensors != 0:
            weighted_avg/=detecting_sensors

        return weighted_avg

    def determine_feature(self, averaged_array_values):
        """
        return 0 on straight, 1 on left L junction, 2 on right L junction,
        3 on T junction or crossing, -1 when off grid
        """
        left   = sum(averaged_array_values[0:2]) / 2
        center = sum(averaged_array_values[2:6]) / 5
        right  = sum(averaged_array_values[6:8]) / 2

        if self.debug:
            print(left, center, right)

        if center < FEAT_THRESHOLD < left and right > FEAT_THRESHOLD:
            if self.debug:
                print("straight")
            return 0
        elif left < FEAT_THRESHOLD and center < FEAT_THRESHOLD < right:
            if self.debug:
                print("left L junction")
            return 1
        elif center < FEAT_THRESHOLD < left and right < FEAT_THRESHOLD:
            if self.debug:
                print("right L junction")
            return 2
        elif left < FEAT_THRESHOLD and center < FEAT_THRESHOLD and right < FEAT_THRESHOLD:
            if self.debug:
                print("T juntion / crossing")
            return 3
        else:
            if self.debug:
                print("off grid or alignment off")
            return -1

    def calibrate(self, buttons):
        """
        calibrates led array sensor
        """
        while True:
            print(self.get_array_values())
            if buttons.enter:
                self.array.command = 'CAL-WHITE'
                print('white calibrated')
                break

        time.sleep(1)

        while True:
            print(self.get_array_values())
            if buttons.enter:
                self.array.command = 'CAL-BLACK'
                print('black calibrated')
                break

    def get_array_values(self):
        """
        returns values from led array sensor
        """
        return [self.array.value(0), self.array.value(1), self.array.value(2), self.array.value(3),
                self.array.value(4), self.array.value(5), self.array.value(6), self.array.value(7)]

    def get_tilt(self):
        """
        returns tilt from imu
        """
        self.imu.mode = 'TILT'
        return [self.imu.value(), self.imu.value(1), self.imu.value(2)]

    def get_compass(self):
        """returns heading from imu
        """
        self.imu.mode = 'COMPASS'
        return self.imu.value()

    def get_gyro(self):
        """
        returns gyro reading from imu
        """
        self.imu.mode = 'GYRO'
        return [self.imu.value(), self.imu.value(1), self.imu.value(2)]

    def get_accel(self):
        """
        returns accelaration from imu
        """
        self.imu.mode = 'ACCEL'
        return [self.imu.value(), self.imu.value(1), self.imu.value(2)]
