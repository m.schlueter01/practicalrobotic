#!/usr/bin/env micropython
"""
main function
"""
import time
import sensors
import movement
import dispatch

from ev3dev2.motor import LargeMotor, OUTPUT_A, OUTPUT_B
from ev3dev2.sensor import Sensor, INPUT_1, INPUT_2, INPUT_3, INPUT_4
from ev3dev2.sensor.lego import ColorSensor, UltrasonicSensor
from ev3dev2.button import Button

SPEED      = 25
NUM_CYCLES = 3
DEBUG      = False

motor_l = LargeMotor(OUTPUT_B)
motor_r = LargeMotor(OUTPUT_A)

array = Sensor(INPUT_1)
color_sens = ColorSensor(INPUT_2)
ultra_sens = UltrasonicSensor(INPUT_3)
imu = Sensor(INPUT_4)

buttons = Button()

sens_ctrl = sensors.SensorController(array, color_sens, ultra_sens, imu, DEBUG)
move_ctrl = movement.MoveController(motor_l, motor_r, array, sens_ctrl, SPEED, DEBUG)
dispatcher = dispatch.Dispatch()

move_ls = [(0, 0), (1, 0), (1, 1), (1, 2), (1, 3), (0, 3)]
queue = dispatch.get_move_queue(move_ls)
print(queue)


while not buttons.left:
    #print(sens_ctrl.get_array_values())
    if buttons.right:
        sens_ctrl.calibrate(buttons)
    time.sleep(0.1)

print("Starting...")
move_ctrl.move_robot()

frame_counter = 1
array_values = [0.0] * 8
value_sum    = [0.0] * 8

while True:
    array_values = sens_ctrl.get_array_values()
    value_sum = [sum(x) for x in zip(array_values, value_sum)]
    if frame_counter % NUM_CYCLES == 0:
        if DEBUG:
            print("Current frame:", frame_counter)
        averaged_array_values = [x / NUM_CYCLES for x in value_sum]
        feat = sens_ctrl.determine_feature(averaged_array_values)
        weighted_avg = sens_ctrl.get_weighted_avg()

        if sum(averaged_array_values) >= 450 and (0 < weighted_avg < 35 or weighted_avg > 55):
            move_ctrl.correct_alignment()
        elif feat == 3: # crossing / t junction
            move_ctrl.turn_robot(True)
        elif feat == 1: # right turn
            print(array_values)
            move_ctrl.turn_robot(False)
        elif feat == 2: # left turn
            move_ctrl.turn_robot(True)
        elif feat == -1: # off grid
            move_ctrl.halt_robot()
            move_ctrl.move_back()
        averaged_array_values = [0.0] * 8

    if buttons.left:
        move_ctrl.move_robot()

    if DEBUG:
        print(sens_ctrl.get_array_values())
        print("-----------------")
        print(color_sens.raw)
        print(ultra_sens.distance_centimeters_continuous)
        print("-----------------")
        print(sens_ctrl.get_accel())
        print(sens_ctrl.get_compass())
        print(sens_ctrl.get_gyro())
        print(sens_ctrl.get_tilt())

    frame_counter += 1
    time.sleep(1/(2*SPEED))
