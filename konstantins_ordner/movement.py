"""
handling of all movement related operations
"""
import time

TIME   = 0.25
ADJ_DEGREES  = 45
TURN_DEGREES = 330

class MoveController:
    """
    controlls all movement of the robot
    """
    def __init__(self, motor_l, motor_r, array, sens_ctrl, speed, debug):
        self.motor_l = motor_l
        self.motor_r = motor_r
        self.motor_l.stop_action = 'hold'
        self.motor_r.stop_action = 'hold'
        self.array   = array
        self.speed   = speed
        self.debug   = debug
        self.sens_ctrl = sens_ctrl

    def correct_alignment(self):
        """
        corrects alignment of the robot wrt the line
        """
        weighted_avg = self.sens_ctrl.get_weighted_avg(self.sens_ctrl.get_array_values(self.array))
        if self.debug:
            print("adjusting alignment")
        if weighted_avg < 35:
            self.halt_robot()
            self.motor_l.on_for_degrees(self.speed, ADJ_DEGREES)
            self.move_robot()
            time.sleep(TIME)
            self.halt_robot()
            array_values = self.sens_ctrl.get_array_values(self.array)
            new_weighted_average = self.sens_ctrl.get_weighted_average(array_values)
            if new_weighted_average <= weighted_avg:
                self.motor_r.on_for_degrees(self.speed, ADJ_DEGREES)
                self.move_robot()
                time.sleep(TIME)
                self.halt_robot()
        elif weighted_avg > 55:
            self.halt_robot()
            self.motor_r.on_for_degrees(self.speed, ADJ_DEGREES)
            self.move_robot()
            time.sleep(TIME)
            self.halt_robot()
            array_values = self.sens_ctrl.get_array_values(self.array)
            new_weighted_average = self.sens_ctrl.get_weighted_average(array_values)
            if new_weighted_average >= weighted_avg:
                self.motor_l.on_for_degrees(self.speed, ADJ_DEGREES)
                self.move_robot()
                time.sleep(TIME)
                self.halt_robot()
        self.move_robot()

    def move_robot(self):
        """
        activates forward movement
        """
        self.motor_l.command = 'run-forever'
        self.motor_l.on(self.speed)
        self.motor_r.command = 'run-forever'
        self.motor_r.on(self.speed)

    def halt_robot(self):
        """
        stops robot completely
        """
        self.motor_l.off()
        self.motor_r.off()

    def turn_robot(self, turn_left):
        """
        turns robot left or right, depending on turn_left bool value
        """
        self.halt_robot()
        if turn_left:
            self.motor_r.on_for_degrees(self.speed, TURN_DEGREES)
            array_values = self.sens_ctrl.get_array_values(self.array)
            weighted_average = self.sens_ctrl.get_weighted_average(array_values)
            while 35 < weighted_average < 55:
                if weighted_average > 55 or weighted_average == 0:
                    self.motor_r.on_for_degrees(self.speed, 10)
                elif weighted_average < 35:
                    self.motor_l.on_for_degrees(self.speed, 5)
                array_values = self.sens_ctrl.get_array_values(self.array)
                weighted_average = self.sens_ctrl.get_weighted_average(array_values)
        else:
            self.motor_l.on_for_degrees(self.speed, TURN_DEGREES)
            array_values = self.sens_ctrl.get_array_values(self.array)
            weighted_average = self.sens_ctrl.get_weighted_average(array_values)
            while 35 < weighted_average < 55:
                if weighted_average < 55 or weighted_average == 0:
                    self.motor_l.on_for_degrees(self.speed, 10)
                elif weighted_average > 35:
                    self.motor_r.on_for_degrees(self.speed, 5)
                array_values = self.sens_ctrl.get_array_values(self.array)
                weighted_average = self.sens_ctrl.get_weighted_average(array_values)
        self.halt_robot()

    def move_back(self):
        """
        moves the robot back until a line is detected again
        """
        self.motor_l.command = 'run-forever'
        self.motor_l.on(-self.speed/2)
        self.motor_r.command = 'run-forever'
        self.motor_r.on(-self.speed/2)
        while self.sens_ctrl.get_weighted_average(self.sens_ctrl.get_array_values(self.array)) == 0:
            time.sleep(0.1)
        self.halt_robot()
