"""
move dispatching & route planning
"""
class Dispatch:
    """
    job dispatching
    """

    def __init__(self):
        return

def get_move_queue(path_list):
    """
    returns move queue based on path list calculated by route planner
    """
    move_queue = []
    orientation = 0

    for i in range(len(path_list)-1):
        if path_list[i][0] == path_list[i+1][0]:
            # we move left or right
            if path_list[i][1] < path_list[i+1][1]:
                # we move right
                if orientation == 0:
                    move_queue.append(1)
                    orientation = 1
                elif orientation == 1:
                    move_queue.append(0)
                elif orientation == 2:
                    move_queue.append(3)
                    orientation = 1
                elif orientation == 3:
                    move_queue.append(2)
            else:
                # we move left
                if orientation == 0:
                    move_queue.append(3)
                    orientation = 3
                elif orientation == 1:
                    move_queue.append(2)
                elif orientation == 2:
                    move_queue.append(1)
                    orientation = 3
                elif orientation == 3:
                    move_queue.append(0)
        else:
            # we move up or down
            if path_list[i][0] > path_list[i+1][0]:
                # we move forward
                if orientation == 0:
                    move_queue.append(0)
                elif orientation == 1:
                    move_queue.append(3)
                    orientation = 0
                elif orientation == 2:
                    move_queue.append(2)
                elif orientation == 3:
                    move_queue.append(1)
                    orientation = 0
            else:
                # we move down
                if orientation == 0:
                    move_queue.append(2)
                elif orientation == 1:
                    move_queue.append(1)
                    orientation = 2
                elif orientation == 2:
                    move_queue.append(0)
                elif orientation == 3:
                    move_queue.append(1)
                    orientation = 2

    return move_queue
