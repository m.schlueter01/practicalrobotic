# %%
import path_algorithem as pa
import stephans_roedelordner as sp
import matplotlib.pyplot as plt

# %%

pathgen = pa.PathAlgo("maze3.ascii")

print(pathgen.P['S'])
print(pathgen.P['G'])

pa.draw_graph(pathgen.A,pathgen.P['S'].shape)

#print("neighbors of (2,1): {}".format(pathgen.A[6]))

print(pathgen.getPath())

#print("neighbors of (2,1): {}".format(pathgen.A[6]))

print(pathgen.updatePos(pa.position((2,2),pathgen.P['S'].shape),"R",pa.position((1,2),pathgen.P['S'].shape)))

#plt.show()
