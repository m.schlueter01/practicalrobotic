# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import numpy as np
from pathlib import Path
import networkx as nx
from copy import deepcopy


# %%
class position:
    """
    A class to store raveled and unraveled representations of a position

    INIT:
        pos <int / iterable>: Position raveled or unraveled (maze coordinates)
        shape <np.array / tuple>:      Reverence shape for un-/raveling

    VALUES:
        self.ravel <int>           graph "coordinates" / node number
        self.unravel <(int, int)>  Maze coordinates
        self.shape <(int, int)>:   Dimensions of maze
    """

    def __init__(self, pos, shape):

        if isinstance(shape, tuple):
            self.shape = shape
        else:
            self.shape = tuple(shape.shape)

        if isinstance(pos, (int, np.integer)):
            self.ravel = int(pos)
            self.unravel = tuple(np.unravel_index(pos, self.shape))

        elif isinstance(pos, tuple):
            self.unravel = pos
            self.ravel = int(self.__ravel(pos))

        else:
            raise ValueError("pos need to be int or tuple")

    def __ravel(self, x):
        return x[0] * self.shape[1] + x[1]

    def __repr__(self):
        return 'P{{{:>5}, {:>3}}}'.format('({},{})'.format(*self.unravel), self.ravel)

    def __eq__(self, other):
        return True if other.ravel == self.ravel else False

    def __hash__(self) -> int:
        return self.ravel


# %%
def read_maze(filename='maze_orig.ascii', test=True):
    """
    Reads maze from ascii file
    Returns maze as binary matrix, adjacency matrix as well as ..
    .. start and stop positions.

    INPUT:

        filename <str>: Text file consisting ' ', '*' and '\n'
        test <bool>:    If true, "filename" might also contain Rs and Bs that will be ..
                        .. replaced by 1s and added to "P".

    OUTPUT:

        mase [np.array]: Array consisting of ints 0 and 1
        A <np.array>:    Adjacency matrix of maze
        P <{str: position / [position, ...]}>: Positions of "S"tart / "G"oal <int> as counted from top left ..
                                .. in test mode also positions of "R"ed and "B"lue blocks <list>
    """
    import numpy as np
    from pathlib import Path
    get_coordinates = lambda maze, letter: list(zip(*np.where(maze == letter)))

    maze = Path(filename).read_text()
    maze = maze.replace(' ', "0").replace('*', "1").strip().split('\n')

    # Fill rows with trailing spaces that had been cut off
    # .. (if line did non end with *)
    maze_width = max([len(row) for row in maze])
    for ii, row in enumerate(maze):
        if len(row) != maze_width:
            maze[ii] += (maze_width - len(row)) * '0'

    # To numpy array
    maze = np.array([[char for char in row] for row in maze])

    # Replace Start and Goal symbols with path elements
    S = get_coordinates(maze, 'S')[0]
    G = get_coordinates(maze, 'G')[0]
    maze[S] = 1
    maze[G] = 1

    # For dry testing of the algorithm, there might also be R and B
    if test is True:
        R = get_coordinates(maze, 'R')
        B = get_coordinates(maze, 'B')
        for (i, j) in R:
            maze[i, j] = 1
        for (i, j) in B:
            maze[i, j] = 1

    maze = maze.astype(int)


    realheight = (maze.shape[0] + 1) // 2
    realwidth = (maze.shape[1] + 1) // 2
    dimensions = (realheight, realwidth)
    # Adjacency matrix needed to create networkx graph
    nodes_count = realheight * realwidth
    A = np.zeros([nodes_count, nodes_count])
    for row in range(h := maze.shape[0]):
        for col in range(w := maze.shape[1]):
            if maze[row, col] == 1:
                if (row % 2) == 1 and (col % 2) == 0:
                    top = ((row-1)//2)*realwidth + (col//2)
                    bot = ((row+1)//2)*realwidth + (col//2)
                    if top >= 0 and bot < nodes_count:
                        A[top,bot] = 1
                        A[bot,top] = 1
                        #print("connect " + str(top) + " and " + str(bot))
                elif (row % 2) == 0 and (col % 2) == 1:
                    left = (row//2)*realwidth + ((col - 1)//2)
                    right = (row//2)*realwidth + ((col + 1)//2)
                    if left >= 0 and right < nodes_count:
                        A[left, right] = 1
                        A[right, left] = 1
                        #print("connect " + str(right) + " and " + str(left))


    P = {'S': position((S[0]//2, S[1]//2), dimensions), 'G': position((G[0]//2, G[1]//2), dimensions)}

    if test is True:
        P['B'] = [position((b[0]//2, b[1]//2), dimensions) for b in B]
        P['R'] = [position((r[0]//2, r[1]//2), dimensions) for r in R]

    graph = nx.convert_matrix.from_numpy_matrix(A)

    # A can from now on be retrieved using nx.adjacency_matrix(graph)
    return maze, graph, P


# %%
def shortest_path(graph, S, G, verbose=False):
    """
    Calculates shortest path from point S to G on basis of adjacency matrix A
    
    INPUT:
    
        graph <nx.graph>: Corresponding graph to maze
        S, G <position>:  Position of Start / Goal as counted from top left
        
    OUTPUT:
    
        positions <[position, ...]>: List of positions to go
    """
    import numpy as np

    path = nx.shortest_path(graph, S.ravel, G.ravel)

    # First one is S
    path = [position(n, S.shape) for n in path][1:]

    if verbose is True:
        print('Found SP:', path)
    return path


# %%
def calc_edge(pos_curr, pos_next):
    """
    Calculate edge in relative maze-indices
    
    INPUT:
        pos_curr <(int, int)>: "maze" coordinates
        pos_next <(int, int)>: "maze" coordinates
        
    OUTPUT:
        edge <[np.ndarray, np.ndarray]>: Edge [pos_curr, pos_next]
    """

    return [pos_curr, pos_next]


# %%
def robot_align_edge(pos_last, pos_curr, pos_next):  # pos_curr, pos_next):
    """
    Align robot to edge
    
    INPUT:
        pos_curr <(int, int)>: "maze" coordinates
        pos_next <(int, int)>: "maze" coordinates
        
    OUTPUT:
        Nothing
    """


#     import numpy as np

#     try:
#         _ = iter(pos_curr)
#     except:
#         raise ValueError(f"{pos_curr} not an iterable")
#     try:
#         _ = iter(pos_next)
#     except:
#         raise ValueError(f"{pos_next} not an iterable")
#     pos_curr = np.array(pos_curr)
#     pos_next = np.array(pos_next)
# Turn robot


# %%
def robot_follow_edge(pos_last, pos_curr, pos_next):
    """
    Align robot to edge
    
    INPUT:
        edge <np.array([change_y, change_x])>
    OUTPUT:
        Nothing
    """

    # Move robot
    def identify_node():
        pass


# %%
def robot_identify_block(pos_curr, P, verbose=False):
    """
    Checks presence and if, color of block
    
    TEST-INPUT:
        pos_curr, P, debug
    
    INPUT:
        Nothing
        
    OUTPUT:
        status <None / str>: None / 'B' / 'R'
    """
    if pos_curr in P['B']:
        if verbose is True:
            print('Found B at', pos_curr)
        return 'B'
    elif pos_curr in P['R']:
        if verbose is True:
            print('Found R at', pos_curr)
        return 'R'
    else:
        if verbose is True:
            print('No block found at', pos_curr)
        return None


# %%
def delete_adj_edges(graph, pos_last, pos_curr, maze=None, verbose=False, removeall=False):
    """
    Delete all adjacent edges from pos_curr but the one to pos_last ...
    ... from graph, A and maze.

	If removeall is True it will also remove the edge to pos_last as well as if if pos_last is None
    
    INPUT:
        graph <nx.graph>: Graph
        maze <np.array>:  Array consisting of ints 0 and 1
        pos_last <position>: "maze" coordinates
        pos_curr <position>: "maze" coordinates
        
    OUTPUT:
        graph <nx.graph>: Graph
        maze <np.array>:  Array consisting of ints 0 and 1
    """
    # Update maze
    # None == out of shape
    if verbose is True:
        print("Remove from graph all adj. nodes of {} but {}".format(pos_curr, pos_last))

    if maze is not None:
        for p in [position((pos_curr.unravel[0] - 1, pos_curr.unravel[1]), maze) if pos_curr.unravel[0] >= 1 else None,
                  position((pos_curr.unravel[0] + 1, pos_curr.unravel[1]), maze) if pos_curr.unravel[0] < maze.shape[
                      0] - 2 else None,
                  position((pos_curr.unravel[0], pos_curr.unravel[1] - 1), maze) if pos_curr.unravel[1] >= 1 else None,
                  position((pos_curr.unravel[0], pos_curr.unravel[1] + 1), maze) if pos_curr.unravel[1] < maze.shape[
                      1] - 2 else None]:
            if p is not None and (pos_last is None or removeall or p.ravel != pos_last.ravel) and maze[p.unravel] == 1:
                maze[p.unravel] = 0
                if verbose is True:
                    print("Removing from maze: {}".format(p))

    # Update graph
    for (i, j) in deepcopy(graph).edges(pos_curr.ravel):
        if (pos_last is None or removeall or pos_last.ravel not in (i, j)):
            graph.remove_edge(i, j)
            if verbose is True:
                print("Removing from graph edge: ({},{})".format(i, j))
            # A[i,j] = 0

    return graph, maze


# %%
def find_neighbours(graph, pos, verbose=False):
    """
    INPUT:
        graph <nx.graph>: Graph
        pos <position>:   Position
        
    OUTPUT:
        <[ (int, int), ... ]>: Neighbours of position in "maze" coordinates
    """
    neighbours = [position(n, pos.shape) for n in graph[pos.ravel]]
    if verbose is True:
        print('Found neighbours:', neighbours)

    return neighbours


# %%
def points_on_straight_line(pos1, pos2):
    pos1 = np.array(pos1.unravel)
    pos2 = np.array(pos2.unravel)
    return True if len(np.nonzero(pos1 - pos2)[0]) == 1 else False


# %%
def follow_edge(pos_last, pos_curr, pos_next, verbose=False):
    # 4.1
    # robot_align_edge(pos_last, pos_curr, pos_next)

    # 4.2-4
    # robot_follow_edge(pos_curr, pos_next)
    if verbose:
        print('===> Going from {} to {}.'.format(pos_curr, pos_next))
    pass


# %%
def construct_graph(graph, new_pos_brick):
    return delete_adj_edges(deepcopy(graph), None, new_pos_brick)[0]


# %%
def point_accessable(graph, source, dest, except_pos=None):
    if (except_pos is not None):
        graph2 = construct_graph(graph, except_pos)
    else:
        graph2 = graph
    return nx.has_path(graph2, source.ravel, dest.ravel)


# %%
def draw_graph(graph, shape):
    posdict = {}
    for n in graph.nodes():
        pos = position(n, shape).unravel
        posdict[n] = (pos[1],shape[0] - pos[0])
    nx.draw(graph, posdict)


# %%
def get_push_source_pos(brick_act, brick_dest):
    brick_act_tuple = brick_act.unravel
    brick_dest_tuple = brick_dest.unravel
    dest = (2 * brick_act_tuple[0] - brick_dest_tuple[0], 2 * brick_act_tuple[1] - brick_dest_tuple[1])
    return position(dest, brick_act.shape)


# %%
def can_push(graph, source, brick_act, brick_dest):
    """
	checks if the player can push the brick on position brick_act to position brick_dest
		
	INPUT:
		graph <nx.graph>: maze graph
		source <position>: position of player
		brick_act <position>: brick position
		brick_dest <position>: brick destination
	"""
    dest = get_push_source_pos(brick_act, brick_dest)
    
    n1 = find_neighbours(graph, brick_act)
    #print("neighbors in can push: {}".format(n1))
    if not n1.__contains__(dest):
        #print("push source {} source not in graph".format(dest))
        return False
    if not n1.__contains__(brick_dest):
        #print("push source {} dest not in graph".format(dest))
        return False
    if not point_accessable(graph, source, dest, brick_act):
        #print("push source {} not accessable".format(dest))
        return False
    #print("push source {} can be pushed".format(dest))
    # this does not check whether there is a brick already on the brick_dest field
    return True


# %%
def get_pushed_brick_path(graph, source, dest, brick_dest):
    g2 = construct_graph(graph, brick_dest)
    if(point_accessable(g2,source,dest)):
        return shortest_path(g2, source, dest)
    return None


# %%
def get_paths_on_movable_brick(graph, source, dest, brick_act):
    """
	checks if the player can push the brick on position brick_act to position brick_dest
		
	INPUT:
		graph <nx.graph>: maze graph
		source <position>: position of player
		dest <position>: player destination position
		brick_act <position>: brick position

	OUTPUT:
		paths <Dictionary<position,[position]>> as key the brick destination position and value the path
	"""
    n1 = find_neighbours(graph, brick_act)
    #print("p1 neighbors of (2,1): {}".format(graph[6]))
    paths = {}
    if point_accessable(graph, source, dest, brick_act):
        #print("p2 neighbors of (2,1): {}".format(graph[6]))
        paths.append(get_pushed_brick_path(graph, source, dest, brick_act))
    #print("p3 neighbors of (2,1): {}".format(graph[6]))
    for n in n1:
        brick_dest = n
        if can_push(graph, source, brick_act, brick_dest):
            push_source = get_push_source_pos(brick_act, brick_dest)
            path_to_push_source = get_pushed_brick_path(graph, source, push_source, brick_act)
            #print("path from {} to {} is {}".format(source,push_source,path_to_push_source))
            path_from_brick_act_to_dest = get_pushed_brick_path(graph, brick_act, dest, brick_dest)
            if path_from_brick_act_to_dest is not None and path_to_push_source is not None:
                whole_path = []
                whole_path += path_to_push_source
                whole_path.append(brick_act)
                #print("to push source {}".format(path_to_push_source))
                whole_path += path_from_brick_act_to_dest
                #print("whole path {}".format(whole_path))
                paths[brick_dest] = whole_path
    return paths


# %%
class PathAlgo:
    def __init__(self, maze_file, start_pos=None, end_pos=None):
        """
		Creates Pathfinding object. It decodes the maze file
		
		INPUT:
			maze_file <str>: File from wich to read the maze
			start_pos <position>: optional start position if not set in maze
			end_pos <position>: optional end position if not set in maze
		"""
        self.maze, self.A, self.P = read_maze(maze_file)
        self.path = shortest_path(self.A, self.P['S'], self.P['G'])

    def getPath(self):
        """
		Returns the Path from Start to End position
		
		OUTPUT:
        	positions <[position, ...]>: List of positions to go
		"""
        return self.path
    def updatePos(self, act_pos, obstackle_type, obstackle_pos):
        """
		Updates the Map and returns the new path the robot should take
		it assumes that the player position is given by the last valid node in the maze
		(which means it is not the node of the brick)
		
		INPUT:
			act_pos <position>: position where robot finds the obstackle (or last valid pos)
			obstackle_type <char>: type of obstackle (either 'B' or 'R')
			obstackle_pos <position>: position of obstackle (or act blocked pos)
			
		OUTPUT:
        	positions <[position, ...]>: updated path
        """
        if obstackle_type == None:
            return self.path
        elif obstackle_type == 'R':
            # it already considers not moving the brick
            paths = get_paths_on_movable_brick(self.A, act_pos, self.P['G'], obstackle_pos)
            best_path_len = 1000
            best_brick_dest = None
            for brick_dest in paths:
                if len(paths[brick_dest]) < best_path_len:
                    best_brick_dest = brick_dest
                    best_path_len = len(paths[best_brick_dest])
            if best_brick_dest is None:
                raise Exception("no path to destination found")
            self.brick_moved_pos = best_brick_dest
            self.tmp_A = construct_graph(self.A, best_brick_dest)
            self.path = paths[best_brick_dest]
            return self.path
        elif obstackle_type == 'B':
            self.A = construct_graph(self.A, obstackle_pos)
            self.path = shortest_path(self.A, act_pos, self.P['G'])
            return self.path
        else:
            raise Exception("no obstackle type {}".format(obstackle_type))